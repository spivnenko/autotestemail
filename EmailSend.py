from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from Option import TestOption

# Класс с параметрами теста
option = TestOption()
# Логин от почты
login = option.email_login
# Пароль от почты
password = option.email_password

# Открыть браузер и перейти на страницу gmail
driver = option.open_browser_chrome(option.url)
# Открыть барузер на весь экран
driver.maximize_window()

# В поле для ввода email, ввести логин
driver.find_element_by_xpath('//*[@id="identifierId"]').send_keys(login)
# Нажать на кнопку Далее
driver.find_element_by_id('identifierNext').click()
# Подождать загрузку страницы
driver.implicitly_wait(5)
# Ввести пароль от почты
driver.find_element_by_xpath('//*[@id="password"]/div[1]/div/div[1]/input').send_keys(password)
# Нажать на кнопку Далее
driver.find_element_by_id('passwordNext').click()

# Подождать пока страница загрузиться
delay = 7 # seconds
try:
    myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'to')))
except TimeoutException:
    print('Loading took too much time!')
# Создать новое сообщение
driver.get('https://mail.google.com/mail/u/0/#inbox?compose=new')
# Указать получетеля
driver.find_element_by_name('to').send_keys('bonj777889@gmail.com')
# Ввести тему письма
driver.find_element_by_name('subjectbox').send_keys('Привет!')
# Нажать на кнопку Отправить
driver.find_element_by_css_selector('#\:8w').click()

'''
driver.close()
'''